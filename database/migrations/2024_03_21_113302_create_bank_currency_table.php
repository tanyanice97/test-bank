<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bank_currency', function (Blueprint $table) {
            $table->id();
            $table->float('bid');
            $table->float('ask');
            $table->timestamp('date_at');
            $table->foreignId('bank_id')->references('id')->on('banks');
            $table->foreignId('currency_id')->references('id')->on('currencies');
            $table->index(['bank_id', 'currency_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bank_currency');
    }
};
