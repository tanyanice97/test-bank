<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Artisan::call('currency:get-list');
        Artisan::call('bank:get-list');
        Artisan::call('branches:get-info-by-bank');
        Artisan::call('currency:get-current-rate');
    }
}
