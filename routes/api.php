<?php

use App\Http\Controllers\BranchesController;
use App\Http\Controllers\RateController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('after')->group(function () {
    Route::get('branches/near', [BranchesController::class, 'getNearBranches']);

    Route::prefix('rate')->group(function () {
        Route::get('average', [RateController::class, 'getAverageRate']);
        Route::get('currency/{slug}', [RateController::class, 'getRateByCurrency']);
    });
});


