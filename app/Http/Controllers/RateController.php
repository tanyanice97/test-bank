<?php

namespace App\Http\Controllers;

use App\Http\Resources\AverageRateCurrenciesResources;
use App\Http\Resources\RateResource;
use App\Models\Currency;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class RateController extends Controller
{
    public function getRateByCurrency(string $slug): ?JsonResponse
    {
        $currency = Currency::where('slug', $slug)->first();

        if ($currency) {
            $data = RateResource::collection($currency->banks);
            $status = 200;
        }

        return response()->json($data ?? [], $status ?? 404);
    }

    public function getAverageRate(): ?JsonResponse
    {
        $currencies = DB::table('bank_currency')
            ->selectRaw('currencies.slug, (AVG(bank_currency.bid)) AS avg_bid, (AVG(bank_currency.ask)) AS avg_ask')
            ->leftJoin('currencies', 'currencies.id', '=', 'bank_currency.currency_id')
            ->groupBy('bank_currency.currency_id')
            ->get();

        return response()->json(['data' => AverageRateCurrenciesResources::collection($currencies)]);
    }
}
