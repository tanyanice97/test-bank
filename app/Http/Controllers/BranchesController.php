<?php

namespace App\Http\Controllers;

use App\Http\Resources\BranchesResource;
use App\Services\RateCurrenciesService;
use Illuminate\Http\Request;

class BranchesController extends Controller
{
    public function __construct(RateCurrenciesService $rateCurrenciesService)
    {
        $this->rateCurrenciesService = $rateCurrenciesService;
    }

    public function getNearBranches(Request $request)
    {
        $branches = BranchesResource::collection($this->rateCurrenciesService->getNearBranches($request));

        return response()->json(['data' => $branches]);
    }
}
