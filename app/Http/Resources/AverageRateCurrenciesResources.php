<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AverageRateCurrenciesResources extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'slug'    => $this->slug,
            'avg_bid' => round($this->avg_bid, 4),
            'avg_ask' => round($this->avg_ask, 4),
        ];
    }
}
