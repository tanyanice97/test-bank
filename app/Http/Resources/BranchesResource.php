<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BranchesResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'name_bank' => $this->bank?->name,
            'name_branch' => $this->name,
            'address' => $this->address,
            'distance' => $this->distance,
            'city' => $this->city
        ];
    }
}
