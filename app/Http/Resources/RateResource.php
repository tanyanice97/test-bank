<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RateResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'name_bank' => $this->name,
            'bid' => $this->pivot->bid,
            'ask' => $this->pivot->ask
        ];
    }
}
