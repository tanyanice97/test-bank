<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Currency extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'slug'];

    public function banks(): BelongsToMany
    {
        return $this->belongsToMany(Bank::class, 'bank_currency')->withPivot(['bid', 'ask']);
    }
}
