<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Bank extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'slug'];

    public function currencies(): BelongsToMany
    {
        return $this->belongsToMany(Currency::class, 'bank_currency')->withPivot(['bid', 'ask']);
    }

    public function branches(): HasMany
    {
        return $this->hasMany(BankBranch::class);
    }
}
