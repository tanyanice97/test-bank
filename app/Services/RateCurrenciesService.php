<?php

namespace App\Services;

use App\Http\Resources\BranchesResource;
use App\Models\BankBranch;
use Illuminate\Http\Request;
use Adrianorosa\GeoLocation\GeoLocation;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;

class RateCurrenciesService
{
    public function getNearBranches(Request $request)
    {
        $ip = $request->ip();
        $details = GeoLocation::lookup($ip);

        $lat = $details->getLatitude();
        $lng = $details->getLongitude();

        if ($lng && $lat) {
           $branches = BankBranch::select('*', DB::raw(sprintf(
                '(6371 * acos(cos(radians(%1$.7f)) * cos(radians(lat)) * cos(radians(lng) - radians(%2$.7f)) + sin(radians(%1$.7f)) * sin(radians(lat)))) AS distance',
                $lat,
                $lng
            )))
                ->with('bank')
                ->having('distance', '<', 10)
                ->orderBy('distance', 'asc')
                ->get();
        }

        return $branches ?? [];
    }
}