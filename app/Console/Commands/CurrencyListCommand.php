<?php

namespace App\Console\Commands;

use App\Models\Currency;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class CurrencyListCommand extends Command
{
    protected $signature = 'currency:get-list';

    protected $description = 'Get list currencies';

    public function handle()
    {
        $response = Http::get('https://minfin.com.ua/api/currency/list?type=money&locale=uk');
        $currencies = ['USD', 'EUR', 'GBP', 'CHF', 'PLN'];

        if ($response->status() === 200 && isset($response->json()['list'])) {
            foreach ($response->json()['list'] as $item) {
                if (in_array($item['slug'], $currencies)) {
                    Currency::updateOrCreate(['slug' => $item['slug']], ['name' => $item['name']]);
                }
            }
        }
    }
}
