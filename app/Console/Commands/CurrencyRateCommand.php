<?php

namespace App\Console\Commands;

use App\Models\Bank;
use App\Models\Currency;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class CurrencyRateCommand extends Command
{
    protected $signature = 'currency:get-current-rate';

    protected $description = 'Get rate by currencies and banks';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $currencies = Currency::get();
        $banks = Bank::select('id', 'slug')->get()->pluck('id', 'slug')->toArray();

        if (count($currencies) > 0 && count($banks) > 0) {
            foreach ($currencies as $currency) {
                $currency->banks()->sync([]);

                $response = Http::get('https://minfin.com.ua/api/currency/rates/banks/' . $currency->slug);
                if ($response->status() === 200 && isset($response->json()['data'])) {
                    foreach ($response->json()['data'] as $item) {
                        if (array_key_exists($item['slug'], $banks)) {
                            $currency->banks()->attach([
                                $banks[$item['slug']] => [
                                    'bid'     => $item['cash']['bid'],
                                    'ask'     => $item['cash']['ask'],
                                    'date_at' => Carbon::parse($item['cash']['date'])->toDateTimeString()
                                ]
                            ]);
                        }
                    }
                }
            }
        }
    }
}
