<?php

namespace App\Console\Commands;

use App\Models\Bank;
use App\Models\Currency;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class BankListCommand extends Command
{
    protected $signature = 'bank:get-list';

    protected $description = 'Get list banks with their info';

    public function handle()
    {
        $currencies = Currency::get();
        if (count($currencies) > 0) {
            $banks = ['privatbank', 'pumb', 'universal-bank', 'ukrsibbank', 'kredobank'];

            foreach ($currencies as $currency) {
                $response = Http::get('https://minfin.com.ua/api/currency/rates/banks/' . $currency->slug);
                if ($response->status() === 200 && isset($response->json()['data'])) {
                    foreach ($response->json()['data'] as $item) {
                        if (in_array($item['slug'], $banks)) {
                            Bank::updateOrCreate(['slug' => $item['slug']], ['name' => $item['name_uk']]);
                        }
                    }
                }
            }
        }
    }
}
