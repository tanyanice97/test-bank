<?php

namespace App\Console\Commands;

use App\Models\Bank;
use App\Models\BankBranch;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class BankBranchesCommand extends Command
{
    protected $signature = 'branches:get-info-by-bank';

    protected $description = 'Get all branches by banks';

    public function handle()
    {
        $banks = Bank::get()->pluck('slug', 'id')->toArray();

        if (count($banks) > 0) {
            foreach ($banks as $key => $bank) {
                $response = Http::get('https://finance.ua/api/organization/v1/branches?slug=' . $bank . '&locale=uk');
                if ($response->status() === 200 && isset($response->json()['data'])) {
                    foreach ($response->json()['data'] as $branch) {
                        foreach ($branch['data'] as $item) {
                            BankBranch::updateOrCreate([
                                'slug'    => $branch['slug'],
                                'bank_id' => $key,
                                'city'    => $branch['name'],
                                'name'    => $item['branch_name'],
                            ], [
                                'address' => $item['address'],
                                'lat'     => $item['lat'],
                                'lng'     => $item['lng']
                            ]);
                        }
                    }
                }
            }
        }
    }
}
